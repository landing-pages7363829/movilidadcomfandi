
<!DOCTYPE html>
<!-- saved from url=(0053)https://finesa-vehiculos.finesa.com.co/muchasgracias/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Comfandi</title>

<meta http-equiv="Cache-Control" content="public">
<meta name="vw96.objectype" content="Document">
<meta name="distribution" content="all">
<meta name="robots" content="all">
<meta name="author" content="Singular Lab">
<meta name="language" content="Spanish">
<meta name="revisit" content="2 days">

<meta property="og:site_name" content="Comfandi">
<meta property="og:type" content="website">
<meta name="zonion" content="muchasgracias">


<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="./FinesaComfandi_files/css2" rel="stylesheet">

<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/all.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/stylesheet.css">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/slick.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/slick-theme.css">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/fontion-2.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/fixion-4.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/cssion.css">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/responsion-3.css">

<link rel="stylesheet" href="./FinesaComfandi_files/fontawesome-free-6.1.1-web/css/all.css">

<script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="./FinesaComfandi_files/analytics.js.descarga"></script><script async="" src="./FinesaComfandi_files/gtm.js.descarga"></script><script src="./FinesaComfandi_files/jquery-3.3.1.min.js.descarga"></script>
<script src="./FinesaComfandi_files/bootstrap.min.js.descarga"></script>
<script src="./FinesaComfandi_files/slick.min.js.descarga"></script>

<link rel="stylesheet" href="./FinesaComfandi_files/jquery.fancybox.min.css">
<script src="./FinesaComfandi_files/jquery.fancybox.min.js.descarga"></script>

<script src="./FinesaComfandi_files/jquery.form.js.descarga"></script>
<script src="./FinesaComfandi_files/jquerion.js.descarga"></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":
new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src=
"https://www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,"script","dataLayer","GTM-W26Z38S");</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-187963105-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K9CYZ4LTH0');
</script>

</head>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W26Z38S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<body>
	<div class="cgeneral">

		<header>
            <div class="ionix bfff" style="padding-bottom: 10px; padding-top: 10px">
                <div class="general">
                    <div class="tab h60 taC_oS">
                        <div class="tabIn pR20_oS">
                                <img src="./FinesaComfandi_files/Logo_comfandi.png" alt="Comfandi Logo">
                        </div>
                        
                    </div>
                </div>
            </div>
        </header>

        <div class="baco1 bFull">

        	<div class="all-content ionix">

        	<div class="cont-thanks">
        		<div class="thankspage">
        			<div class="icono-thanks"><i class="fas fa-check"></i></div>
        			<h1 class="">¡Gracias por preferirnos!</h1>
        			<h3 class="">Gracias por tu registro, en este momento tu solicitud no podrá ser aprobada por nuestras políticas de crédito. Esperamos poder atenderte 
						más adelante. Actualmente esta opción de crédito aplica para ingresos superiores a 2.000.000</h3>
        			<h5>Recuerda que nuestros horarios de atención son de lunes a viernes de 8:00 a.m. a 5:00 p.m.</h5>

        			<div>
        				<a href="./" class="">
        					<div class="boton-thank">
        						<i class="fas fa-arrow-left" style="margin-right: 10px;"></i>
        						<span class="text-boton">Volver</span>
        					</div>
        				</a>
                    </div>

        		</div>
        	</div>
        </div>

       </div>
    </div>

    <footer>
        <div class="  bAzul pAA20_oS ">
            <div class="ionix footer">
            <div class="social-media">
                <div class="icono instagram"><a href="https://www.instagram.com/finesacol/?hl=es-la" target="_blank"><i class="fab fa-instagram"></i></a></div>
                <div class="    icono facebook"><a href="https://www.facebook.com/Finesa-Veh%C3%ADculos-102249921814555" target="_blank" ><i class="fab fa-facebook-f"></i></a></div>
                <div class="icono linkedin"><a href="https://www.linkedin.com/company/finesa-s-a/mycompany/" target="_blank" ><i class="fab fa-linkedin-in"></i></a></div>
            </div>
            <div class="derechos" > 
                    <span>Todos los derechos reservados © 2022</span>
            </div>  
                    
            <div class="logo-finesa">                           
                <img src="./FinesaComfandi_files/Logo_Finesa_tinta.png" alt="FinesaLogo">                                    
            </div>
            </div>
        </div>
    </footer>


	<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","900658374034308");fbq("set","agent","tmgoogletagmanager","900658374034308");fbq("track","PageView");</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=900658374034308&amp;ev=PageView&amp;noscript=1"></noscript>
</body>
</html>