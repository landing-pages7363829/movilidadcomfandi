<?php

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=registros-landing-comfandi" . date('Y-m-d') . ".xls");
header("Pragma: no-cache");
header("Expires: 0");

include("db.php");
include('../static/index.php');
include('../controlles/app.php');
?>

<?php
  session_start();
      
    $_ZOOM = new Zoom();
    $consulta   = "";
    $lim        = "";

    $_SESSION['desde'];
    $_SESSION['hasta'];
                         


    if(isset($_SESSION['desde']) && ($_SESSION['hasta']) && $_SESSION['desde'] != "" && $_SESSION['hasta'] != ""){

        $desde      = $_SESSION["desde"];
        $hasta      = $_SESSION["hasta"];
        
        $hasta1 = new DateTime($hasta);
       
        
        $consulta   = " AND fecha BETWEEN '".$desde."' AND '".$hasta1->format('Y-m-d')."' ";

    } else $lim = " LIMIT 20 ";

    $listados = $_ZOOM->get_data("contactocomfandi", $consulta . "  " . $lim, 1);
    if($listados) {

?>
    

    <table id="" class="table table-striped table-bordered">
        <?php $k = 1;
        foreach ($listados as $listado) {
            if ($k == 1) { ?>
                <tr>
                    <th>Ip</th>
                    <th>Referente</th>
                    <th>Fecha</th>
                    <th>Nombre</th>
                    <th>Documento</th>
                    <th>Ciudad</th>
                    <th>Email</th>
                    <th>Celular</th>
                    <th>Rango</th>
                    <th>Carro</th>
                    <th>Encuentra</th>
                </tr>
        <?php }
            $k++;
        } ?>
        <tbody>
            <?php foreach ($listados as $listado) { ?>


                
            <tr>
                <td><?= $listado["ip"]; ?></td>
                <td><?= $listado["id_referente"]; ?></td>
                <td><?= $listado["fecha"]; ?></td>
                <td><?= ($listado["name"]); ?></td>
                <td><?= ($listado["identity"]); ?></td>
                <td><?= ($listado["city"]); ?></td>
                <td><?= ($listado["email"]); ?></td>
                <td><?= ($listado["phone"]); ?></td>
                <td><?= ($listado["rango"]); ?></td>
                <td><?= ($listado["carro"]); ?></td>
                <td><?= ($listado["encuentra"]); ?></td>
            </tr>
        <?php } ?>
                <td><strong>Desde</strong></td>
                <td><?= $_SESSION['desde']; ?></td>
                <td><strong>Hasta</strong></td>
                <td><?= $_SESSION['hasta'];?></td>
            </tr>
        </tbody>
    </table>

<?php } ?>