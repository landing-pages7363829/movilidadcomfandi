<?php
session_start();
if (isset($_SESSION['user_id'])) {
} else {
  die();
}
?>


<?php include("../static/index.php"); ?>
<?php include("../static/navbar.php"); 

  if(isset($_SESSION['message'])){
      $mess=$_SESSION['message'];
    echo "<p class='alert alert-primary'>'".$mess."'</p>";
  }
?>



<div class="container">
  <div class=" row">
    <h1 class="text-center">Registrar nuevos usuarios</h1>

    <form action="registro.php" method="POST">

      <div class="mb-3 ">
        <input class="form-control " name="email" id="email" type="text" required="" placeholder="Ingrese tu email">
      </div>
      <div class="mb-3">
        <input class="form-control" name="password" id="password" type="password" required=""  placeholder="Ingrese tu contraseña">
      </div>

      <div class="mb-3">
        <input class="form-control" name="confirm_password" id="confirm_password"  type="password" required=""  placeholder="Confirme su contraseña">
      </div>
      <div class="mb-3">
        <input class="btn btn btn-primary" id="button" type="submit" value="Ingresar">

    </form>


  </div>

</div>


<script text= "text/javascript">

let input = document.querySelector("#email");
let button = document.querySelector("#button");
button.disabled = true;
input.addEventListener("change", stateHandle );
function stateHandle() {
  if (input.value === "" ) {
    
    button.disabled = true; 
  } else {
    button.disabled = false;
  }
}


</script>


</body>

</html>