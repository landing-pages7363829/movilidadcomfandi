
<div class="content-body">
    <div class="card">
        <div class="card-content collapse show">
            <div class="card-header">
                <h4 class="card-title">Filtrar registros por rango de fechas</h4>
            </div>
            <div class="card-body">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Desde</label>
                            <input type="date" class="form-control" id="desde" name="desde" value="<?= $desde; ?>" required>
                        </div>
                        <div class="col-md-4">
                            <label>Hasta</label>
                            <input type="date" class="form-control" id="hasta" name="hasta" value="<?= $hasta; ?>" required>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" style="color:white">Buscar / Filtrar</label>
                            <input type="submit" value="Buscar / Filtrar" class="btn btn-block btn-primary" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>