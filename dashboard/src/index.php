<?php

session_start();

if (isset($_SESSION['user_id'])) {
}
require './db/db.php';


if (!empty($_POST['email']) && !empty($_POST['pass'])) {
  $records = $conn->prepare('SELECT id, email, clave FROM users WHERE email = :email');
  $records->bindParam(':email', $_POST['email']);
  $records->execute();
  $results = $records->fetch(PDO::FETCH_ASSOC);

  $message = '';

  if (is_countable($results) > 0 && password_verify($_POST['pass'], $results['clave'])) {
    $_SESSION['user_id'] = $results['id'];


    header("Location: ./static/exportado.php");
  } else {
    $message = 'Lo siento no esta registrados';
  }
}

?>
<!--Pagina login  -->
<?php
include "./static/index.php";
?>

<div class="container">
  <div class="row">
    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div class="card border-0 shadow rounded-3 my-5">
        <div class="card-body p-4 p-sm-5">
          <a href="https://finesa.com.co/" style="display:flex; flex-direction:row; justify-content:center; ">
						<img src="logo_finesa.svg" alt="Finesa Logo">
					</a><br><br>
          <!-- <h5 class="card-title text-center mb-5 fw-light fs-5">Comfandi Admin</h5> -->
          <form action="" method="POST">
            <div class="form-floating mb-3">
              <input type="email" name="email" class="form-control" id="floatingInput" placeholder="iengrese su correo">
              <label for="floatingInput">Correo</label>
            </div>
            <div class="form-floating mb-3">
              <input type="password" name="pass" class="form-control" id="floatingPassword" placeholder="Ingrese su contraseña">
              <label for="floatingPassword">Contraseña</label>
            </div>
            <div class="d-grid">
              <button class="btn btn-primary btn-login text-uppercase fw-bold" type="submit">Entrar
              </button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<style>
  body {
    background: #007bff;
    background: linear-gradient(to right, #ffff, #000000);
  }

  .btn-login {
    font-size: 0.9rem;
    letter-spacing: 0.05rem;
    padding: 0.75rem 1rem;
    /* background-color: #FC723F; */
    border: none;
  }

  .btn-login:hover {
    background-color: red;
  }
</style>
</body>

</html>