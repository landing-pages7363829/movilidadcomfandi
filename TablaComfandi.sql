

--contactocomfandi(ip,name,identity,fnacimiento,city,fexpedicion,phone,email,rango,carro,encuentra,terms,privacy,fecha

CREATE TABLE `contactocomfandi` (
  `id` int(11) NOT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `identity` varchar(100) DEFAULT NULL,
  `fnacimiento` date DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `fexpedicion` date DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `rango` varchar(100) DEFAULT NULL,
  `carro` varchar(100) DEFAULT NULL,
  `encuentra` varchar(5) DEFAULT NULL,
  `terms` int(11) DEFAULT NULL,
  `privacy` int(11) DEFAULT NULL,
  `fecha` varchar(100) DEFAULT NULL  
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- Indices de la tabla `contactocomfandi`
--
ALTER TABLE `contactocomfandi`
  ADD PRIMARY KEY (`id`);


--
-- AUTO_INCREMENT de la tabla `contactocomfandi`
--
ALTER TABLE `contactocomfandi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


