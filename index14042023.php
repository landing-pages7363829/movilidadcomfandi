<?php
    function getRealIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        return $_SERVER['REMOTE_ADDR'];
    }

?>


<!DOCTYPE html>

<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Crédito de Vehículo Comfandi</title>
<link rel="icon" type="image/x-icon" href="./FinesaComfandi_files/Favicon.png">

<meta http-equiv="Cache-Control" content="public">
<meta name="vw96.objectype" content="Document">
<meta name="distribution" content="all">
<meta name="robots" content="all">
<meta name="author" content="Finesa">
<meta name="language" content="Spanish">
<meta name="revisit" content="2 days">

<meta property="og:site_name" content="Finesa Comfandi">
<meta property="og:type" content="website">
<meta name="zonion" content="index.php">


<meta name="title" content="¡Con Comfandi estás más cerca de tener tu carro!">
<meta property="og:title" content="¡Con Comfandi estás más cerca de tener tu carro!">
<meta name="description" content="Financiamos a tu medida">
<meta property="og:description" content="Financiamos a tu medida">
<meta property="og:image" itemprop="image" content="./FinesaComfandi_files/img-link.png">
<meta name="keywords" content="Comfandi, Credito para vehiculo,credito para vehiculo usado,Credito vehiculo,Credito de vehiculo,Carros usados a credito,Financiacion para carros,Financiacion para autos usados,Financiacion para autos,Financiacion para vehiculos usados,Financiacion para vehiculos">


<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
<link href="./FinesaComfandi_files/css2" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/all.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/stylesheet.css">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/slick.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/slick-theme.css">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/fontion-2.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/fixion-4.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/cssion.css">
<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/style_btn.css">

<link rel="stylesheet" type="text/css" href="./FinesaComfandi_files/responsion-3.css">

<link rel="stylesheet" href="./FinesaComfandi_files/fontawesome-free-6.1.1-web/css/all.css">


<script src="./FinesaComfandi_files/jquery-3.3.1.min.js.descarga"></script>
<link rel="stylesheet" href="./FinesaComfandi_files/jquery.fancybox.min.css">
<script src="./FinesaComfandi_files/jquery.fancybox.min.js.descarga"></script>
<script src="./FinesaComfandi_files/jquery.form.js.descarga"></script>
<!--<script src="./FinesaComfandi_files/jquerion.js.descarga"></script>-->

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/20547272.js"></script>
<!-- End of HubSpot Embed Code -->



<!-- carga select -->
<link rel="stylesheet" type="text/css" href="select2/select2.min.css">
	<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
	<script src="select2/select2.min.js"></script>
<!-- fin select lib -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-187963105-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K9CYZ4LTH0');
</script>

</head>

<body>
    <div class="cgeneral">
        
        <header>
            <div class="ionix bfff" style="padding-bottom: 10px; padding-top: 10px">
                <div class="general">
                    <div class="tab h60 taC_oS">
                        <div class="tabIn pR20_oS">
                            <a href="https://www.comfandi.com.co/">
                            <img src="./FinesaComfandi_files/Logo_comfandi.png" alt="Comfandi Logo">
                            </a>
                        </div>

                        <div class="boton pPay" style="width: 160px;padding: 16px 50px 41px 10px; margin-right: 95px;"> 
                        <a href="https://pagos.movilidadcomfandi.com.co/">  
                            
                                <span class="text-boton" style=" padding: 1px 100px 7px 20px; height: 31px; width: 40px;">PAGAR </span>
        					
        				</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="posR bFull baco1">

            <div class="all-content ionix">

                <div class="legal-comfandi-izq">
                 <span>La Caja de Compensación Familiar Comfandi, miembro de la Asociación de Cajas de Compensación Familiar (Asocajas), le genera bienestar social a los trabajadores de medios y bajos ingresos y a sus familias.</span>
                </div>
                <div class="legal-comfandi-der">
                    <img src="./FinesaComfandi_files/Vigilado.png">
                </div>

 
            <div class="ctexto">
                <div class="exxt">
                    <div>
                        <h2 class="dB t32 "  style=" "> <b style="color:#F7FF5E;">Crédito de Vehículo Comfandi</b> </h2>
                        <h3 class="dB t20 ff1 mb50 "><b style="color:#FFFFFF;">¡Un crédito que te hará mover!</b> </h3>
                        <h4 class="dB t16 ff1 mb50 " style="color:#FFFFFF; margin-top: 20px;"> Te ayudamos a tener el carro que quieres con financiación hasta el 100%. </h4>
                    </div>
                </div>

                <div class="pLeftEsp1 pLR32_oS posR exxt1 web" style="z-index: 1; ">
                    <div class="exxt2" >
                        <span class="Aprobacion">Aprobación en 48 horas.</span>
                        <span class="Categoria">Tasa Subsidiada por categoría hasta 4.5% EA*.</span>
                        <span class="Tasa">Tasa fija desde 0,92%.</span>
                        <span class="Meses">Plazos de hasta 84 meses.</span>
                        <span class="Seguro">Seguro de vida $420 por millón.</span>
                        <span class="Dinero">Ahorro de $57.090* en inscripción al RUNT y Confecámaras.</span>
                        <span class="Persona">Puedes aplicar con ingresos familiares.</span>
                    </div>
                </div>

                <div class="car-img web">
                    <!-- <img src="./FinesaComfandi_files/Carro.png" alt="carro"> -->
                </div>
            </div>

            <div class="sidebar cformulario" id="fom">
                <div class="man-img">
                    <img src="./FinesaComfandi_files/Hombre.png" alt="hombre">
                </div>

                <div class="formulario  maxHLat">
                    <h4 class="titulo-form">Si estás interesado en crédito de vehículo, ingresa tus datos y te contactaremos.</h4>
                    <form  action="contactcomfandiMailer.php" id="contactlanding" name="contact" method="post" class="form-horizontal zoom_form">
                    <?php if(isset($_GET["ref"])) $code = $_GET["ref"]; else $code = "Comfandi"; ?>
                    <input name="id_referente" type="hidden" value="<?= $code; ?>" />
                        <input name="ip" type="hidden" value="<?= getRealIP(); ?>" />

                        <div class="input-form">
                            <div class="label-input nombre">
                                <div class="input-container">
                                    <i class="fa fa-user icon"></i>
                                    <input id="name" name="name" type="text" maxlength="50" required="" class="input-field" onkeyup="validarletras(this)">
                                </div>
                            </div> 

                                <?php
                                    // fecha de nacimiento
                                    $fmaxY = date("Y")-18;
                                    $fminY_naci = date("Y")-100;
                                    $fmaxM = date("m");
                                    $fmaxD = date("d");
                                    $fmax  = $fmaxY."-".$fmaxM."-".$fmaxD;
                                    $fmin_naci  = $fminY_naci."-".$fmaxM."-".$fmaxD;

                                    // fecha de expedicion
                                    $fminY = date("Y")-62; // fecha mayor de edad con max de 80
                                    $fmaxY = date("Y");
                                    $fmin_exp = $fminY."-".$fmaxM."-".$fmaxD;
                                    $fmax_exp = $fmaxY."-".$fmaxM."-".$fmaxD;
                                ?>

                                <div class="input-container-doble">
                                    <div class="label-input fnacimiento">
                                        <div class="input-container">
                                            <i class="fa fa-calendar icon"></i>
                                            <input id="fnacimiento" name="fnacimiento" type="date" required="" class="input-field"  min="<?php  echo $fmin_naci; ?>" max="<?php  echo $fmax; ?>">
                                        </div>
                                    </div>

                                    <div class="label-input city">
                                        <div class="input-container">
                                            <i class="fa fa-map-marker icon"></i> 
                                                                                        
                                            <select id="city" name="city" required="" class="form-select" aria-label="Default select example">
                                            <option value="">Selecciona una opción...</option>
                                            </select>
                                            
                                                <script type="text/javascript">
                                                    function cargar_citys()
                                                    {                                                        
                                                       fetch('./FinesaComfandi_files/json/ciudades.json')
                                                        .then(response => response.text())
                                                        .then(Lciudades => { var array= JSON.parse(Lciudades); 
                                                         ciudad = array.sort()  

                                                        for(var i in ciudad)
                                                        { 
                                                            var jciudad = ciudad[i].ciudades.length;                                                             
                                                            document.getElementById("city").innerHTML += "<optgroup label='"+ ciudad[i].departamento+"'></optgroup>"; 
                                                             
                                                            for(var j = 0; j < jciudad; j++){
                                                                document.getElementById("city").innerHTML += "<option value='"+ciudad[i].ciudades[j]+"'>"+ciudad[i].ciudades[j]+"</option>"; 

                                                            }
                                                        }
                                                        });
                                                    }

                                                cargar_citys();
                                                </script>

                                        </div>
                                    </div>
                                </div>

                                <div class="input-container-doble">
                                    <div class="label-input number">
                                        <div class="input-container ">
                                            <i class="fa fa-id-card icon"></i>
                                            <input id="identity" name="identity" type="text" pattern=".{6,}" maxlength="10" required="" class="input-field" onkeyup="validarNumeros(this)">
                                        </div>
                                    </div>

                                    <div class="label-input fexpedicion">
                                        <div class="input-container" >
                                            <i class="fa fa-calendar icon"></i> 
                                            <input id="fexpedicion" name="fexpedicion" type="date" required="" class="input-field" max="<?php  echo $fmax_exp; ?>" min="<?php  echo $fmin_exp; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="input-container-doble">
                                    <div class="label-input celular">
                                        <div class="input-container">
                                            <i class="fa fa-phone icon"></i>
                                            <input id="phone" name="phone" type="text" pattern=".{6,}" maxlength="10" required="" class="input-field" onkeyup="validarNumeros(this)">
                                        </div>
                                    </div>

                                    <div class="label-input email">
                                        <div class="input-container" >
                                            <i class="fa fa-envelope icon"></i> 
                                            <input id="email" name="email" type="email" required="" class="input-field">
                                        </div>  
                                    </div>
                                </div>

                                <div class="input-container-doble">
                                    <div class="label-input rango">
                                        <div class="input-container">
                                            <i class="fa fa-dollar-sign icon"></i>
                                            <select id="rango" name="rango" required="" class="form-select" aria-label="Default select example">
                                            <option value="">Selecciona una opción...</option>
                                            <option value="Menos de $2.000.000">Menos de  2.000.000 </option>
                                            <option value="De $2.000.000 a $4.000.000">De $2.000.000 a $4.000.000</option>
                                            <option value="De $4.000.000 a $6.000.000">De $4.000.000 a $6.000.000</option>
                                            <option value="Más de $6.000.000">Más de $6.000.000</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="label-input carro">
                                        <div class="input-container" >
                                            <i class="fa fa-car icon"></i>
                                            <select id="carro" name="carro" required="" class="form-select" aria-label="Default select example">
                                                <option value="">Selecciona una opción...</option>
                                                <option value="Automóvil">Automóvil</option>
                                                <option value="Camioneta o campero">Camioneta o campero</option>
                                                <option value="moto">Moto</option>
                                                <option value="Bicicleta electrica">Bicicleta eléctrica </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="label-input encuentra">
                                    <div class="input-container">
                                        <i class="fa fa-question icon"></i>
                                        <select id="encuentra" name="encuentra" required="" class="form-select w100 placeh" aria-label="Default select example">
                                            <option value="">Selecciona una opción...</option>
                                            <option value="Sí">Sí</option>
                                            <option value="No">No</option>
                                        </select> 
                                    </div>
                                </div>
                            </div>

                            <div class="politicas">
                                <div class="politicas-card">
                                    <div class="politicas-check">
                                        <input name="terms" id="terms" class="cP" type="checkbox" required="" value = "1">
                                    </div>

                                    <div class="politicas-text">
                                        <p>Al dar clic, declaro que he leído y acepto la <a href="https://www.comfandi.com.co/legal" target="_blank" class="terms">política de privacidad de Comfandi </a> y consiento el tratamiento de mis datos personales como <a href="https://www.comfandi.com.co/autorizacion-tratamiento-de-datos-personales" target="_blank" class="terms">prospecto y cliente de los programas y/o servicios de Crédito.</a></p>
                                    </div>
                                </div>

                                <div class="politicas-card">
                                    <div class="politicas-check">
                                        <input name="privacy" id="privacy" class="cP" type="checkbox" required="" value = "1">
                                    </div>

                                    <div class="politicas-text">
                                        <p>Al dar clic, declaro que he leído, acepto y autorizo la <a href="./documentos/Autorizacion_para_consultas_y_reporte.pdf" target="_blank" class="terms">consulta y reporte a centrales de riesgo.</a></p>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <button class="boton" type="submit" id="btn-contact" name="enviar">
                                    <div class="cbotonoff" id="btnContainer">
                                    <div class="text-boton">ENVIAR</div>
                                    </div>
                                </button>
                                
                                <div text-align='center'>
                                    <div id="button" class="preloader hide ">
                                        <div class="bar-container">
                                            <div class="bar"></div>
                                            <div class="bar"></div>
                                            <div class="bar"></div>
                                        </div>
                                    </div>
                                    </div>
                                <style type="text/css">
                                .hide{
                                    display:none;
                                    margin: 0px auto !important;
                                }
                                .border{
                                    border:none !important;
                                }
                            </style>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#contactlanding").on("submit" ,function(){
                                        $("#btn-contact").hide();
                                        $("#button").removeClass("hide");
                                    });
                                });                         

                            </script>
                                
                            </div>
                        </form>
                    </div>
                </div>

                <div class="pLeftEsp1 pLR32_oS posR exxt1 phone" style="z-index: 1; ">
                    <div class="exxt2" >
                    <span class="Aprobacion">Aprobación en 48 horas.</span>
                        <span class="Categoria">Tasa Subsidiada por categoría hasta 4.5% EA*.</span>
                        <span class="Tasa">Tasa fija desde 0,92%.</span>
                        <span class="Meses">Plazos de hasta 84 meses.</span>
                        <span class="Seguro">Seguro de vida $420 por millón.</span>
                        <span class="Dinero">Ahorro de $57.090* en inscripción al RUNT y Confecámaras.</span>
                        <span class="Persona">Puedes aplicar con ingresos familiares.</span>
                    </div>
                </div>
                <div class="car-img phone">
                    <img src="./FinesaComfandi_files/Carro.png" alt="carro">
                </div>
            </div>

            </div>
        </div>

    <footer>
        <div class="  bAzul pAA20_oS ">
            <div class="ionix footer">
            <div class="social-media">
                <div class="icono instagram"><a href="https://www.instagram.com/finesacol/?hl=es" target="_blank"><i class="fab fa-instagram"></i></a></div>
                <div class="icono facebook"><a href="https://www.facebook.com/finesacol" target="_blank" ><i class="fab fa-facebook-f"></i></a></div>
                <div class="icono linkedin"><a href="https://www.linkedin.com/company/finesacol/mycompany/" target="_blank" ><i class="fab fa-linkedin-in"></i></a></div>
            </div>
            <div class="derechos" > 
                    <span>Todos los derechos reservados © 2023</span>
            </div>  
                    
            <div class="logo-finesa">                           
                <img src="./FinesaComfandi_files/Logo_Finesa_tinta.png" alt="FinesaLogo">                                    
            </div>
            </div>
        </div>
    </footer>

    
<script src="./FinesaComfandi_files/js/general.js"></script>
<script src="./FinesaComfandi_files/json/ciudades.json"></script>

<script>
    // este codigo bloquea el boton enviar e remplazado por el preloader
      /*  $(document).ready(function() {
        $("#contact").on('submit', function(e) {
            if ($("#terms").is(':checked') && $('#privacy').is(':checked')) {
                if ($("#btnContainer").hasClass('cbotonoff')) {
                    $("#btn-contact")
                        .removeClass('boton')
                        .addClass('noboton');
                    
                    $("#btnContainer")
                        .removeClass('cbotonoff')
                        .css('background-color', '#dbdbdb');
                    $(this).prop('disabled', true);
                } else {
                    e.preventDefault();
                }
            } else {
                e.preventDefault();
            }

        });

        });
        //  fin bloqueo control
        */

        // limpia index
        function borra() {
            let boton = document.querySelector("#btn-contact");
            let formulario = document.querySelector("#contactlanding");
            let carga = document.querySelector("#button");
            document.addEventListener("submit", ()=>{
                setTimeout(()=>{
                    formulario.reset();
                    carga.classList.add("hide");
                    boton.style.display='block';                                       

                },6000);

            });

        };
        borra();

</script>

</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('#city').select2();
	});
</script>